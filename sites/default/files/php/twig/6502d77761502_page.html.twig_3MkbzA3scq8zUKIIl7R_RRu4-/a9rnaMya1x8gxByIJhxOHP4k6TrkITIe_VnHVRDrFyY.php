<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/startup_zymphonies_theme/templates/layout/page.html.twig */
class __TwigTemplate_c0b1402cadef9a208423c63695241cfbab2924ee45cffc6b5d0d12a1564ced0c extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 71, "for" => 100];
        $filters = ["escape" => 72, "raw" => 101, "date" => 518];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['if', 'for'],
                ['escape', 'raw', 'date'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 60
        echo "
<div class=\"header\">
  <div class=\"container\">
    <div class=\"row\">

      <!-- Start: Header -->

      <div class=\"navbar-header col-md-3\">
        <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#main-navigation\">
          <i class=\"fas fa-bars\"></i>
        </button>
        ";
        // line 71
        if ($this->getAttribute(($context["page"] ?? null), "header", [])) {
            // line 72
            echo "          ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header", [])), "html", null, true);
            echo "
        ";
        }
        // line 74
        echo "      </div>

      <!-- End: Header -->

      ";
        // line 78
        if (($this->getAttribute(($context["page"] ?? null), "primary_menu", []) || $this->getAttribute(($context["page"] ?? null), "search", []))) {
            // line 79
            echo "        <div class=\"col-md-9\">

          ";
            // line 81
            if ($this->getAttribute(($context["page"] ?? null), "search", [])) {
                // line 82
                echo "            ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "search", [])), "html", null, true);
                echo "
          ";
            }
            // line 84
            echo "
          ";
            // line 85
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "primary_menu", [])), "html", null, true);
            echo "

        </div>
      ";
        }
        // line 89
        echo "
      </div>

    </div>
  </div>
</div>


";
        // line 97
        if ((($context["is_front"] ?? null) && ($context["show_slideshow"] ?? null))) {
            // line 98
            echo "  <div class=\"flexslider\">
    <ul class=\"slides\">
      ";
            // line 100
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["slider_content"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["slider_contents"]) {
                // line 101
                echo "        ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->sandbox->ensureToStringAllowed($context["slider_contents"]));
                echo "
      ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['slider_contents'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 103
            echo "    </ul>
  </div>
";
        }
        // line 106
        echo "

<!-- Start: Top widget -->

";
        // line 110
        if ((($this->getAttribute(($context["page"] ?? null), "topwidget_first", []) || $this->getAttribute(($context["page"] ?? null), "topwidget_second", [])) || $this->getAttribute(($context["page"] ?? null), "topwidget_third", []))) {
            // line 111
            echo "  <div class=\"topwidget\" id=\"topwidget\">
    <div class=\"container\">
        <div class=\"row clearfix\">

          <!-- Start: Top widget first -->
          ";
            // line 116
            if ($this->getAttribute(($context["page"] ?? null), "topwidget_first", [])) {
                // line 117
                echo "            <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["topwidget_class"] ?? null)), "html", null, true);
                echo ">";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "topwidget_first", [])), "html", null, true);
                echo "</div>
          ";
            }
            // line 119
            echo "          <!-- End: Top widget first -->

          <!-- Start: Top widget second -->
          ";
            // line 122
            if ($this->getAttribute(($context["page"] ?? null), "topwidget_second", [])) {
                // line 123
                echo "            <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["topwidget_class"] ?? null)), "html", null, true);
                echo ">";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "topwidget_second", [])), "html", null, true);
                echo "</div>
          ";
            }
            // line 125
            echo "          <!-- End: Top widget second -->

          <!-- Start: Top widget third -->
          ";
            // line 128
            if ($this->getAttribute(($context["page"] ?? null), "topwidget_third", [])) {
                // line 129
                echo "            <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["topwidget_class"] ?? null)), "html", null, true);
                echo ">";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "topwidget_third", [])), "html", null, true);
                echo "</div>
          ";
            }
            // line 131
            echo "          <!-- End: Top widget third -->

          <!-- Start: Top widget forth -->
          ";
            // line 134
            if ($this->getAttribute(($context["page"] ?? null), "topwidget_forth", [])) {
                // line 135
                echo "            <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["topwidget_class"] ?? null)), "html", null, true);
                echo ">";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "topwidget_forth", [])), "html", null, true);
                echo "</div>
          ";
            }
            // line 137
            echo "          <!-- End: Top widget third -->

        </div>
    </div>
  </div>
";
        }
        // line 143
        echo "
<!--End: Top widget -->


<!--Start: Highlighted -->

";
        // line 149
        if ($this->getAttribute(($context["page"] ?? null), "highlighted", [])) {
            // line 150
            echo "  <div class=\"highlighted\">
    <div class=\"container\">
      ";
            // line 152
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "highlighted", [])), "html", null, true);
            echo "
    </div>
  </div>
";
        }
        // line 156
        echo "
<!--End: Highlighted -->

<!--Start: Title -->

";
        // line 161
        if (($this->getAttribute(($context["page"] ?? null), "page_title", []) &&  !($context["is_front"] ?? null))) {
            // line 162
            echo "  <div id=\"page-title\">
    <div id=\"page-title-inner\">
      <div class=\"container\">
        ";
            // line 165
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "page_title", [])), "html", null, true);
            echo "
      </div>
    </div>
  </div>
";
        }
        // line 170
        echo "
<!--End: Title -->

<div class=\"main-content\">
  <div class=\"container\">
    <div class=\"\">

      <!--Start: Breadcrumb -->

      ";
        // line 179
        if ( !($context["is_front"] ?? null)) {
            // line 180
            echo "        <div class=\"row\">
          <div class=\"col-md-12\">";
            // line 181
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "breadcrumb", [])), "html", null, true);
            echo "</div>
        </div>
      ";
        }
        // line 184
        echo "
      <!--End: Breadcrumb -->

      <div class=\"row layout\">

        <!--- Start: Left SideBar -->
        ";
        // line 190
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])) {
            // line 191
            echo "          <div class=";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["sidebarfirst"] ?? null)), "html", null, true);
            echo ">
            <div class=\"sidebar\">
              ";
            // line 193
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])), "html", null, true);
            echo "
            </div>
          </div>
        ";
        }
        // line 197
        echo "        <!-- End Left SideBar -->

        <!--- Start Content -->
        ";
        // line 200
        if ($this->getAttribute(($context["page"] ?? null), "content", [])) {
            // line 201
            echo "          <div class=";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["contentlayout"] ?? null)), "html", null, true);
            echo ">
            <div class=\"content_layout\">
              ";
            // line 203
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
            echo "
            </div>
          </div>
        ";
        }
        // line 207
        echo "        <!-- End: Content -->

        <!-- Start: Right SideBar -->
        ";
        // line 210
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])) {
            // line 211
            echo "          <div class=";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["sidebarsecond"] ?? null)), "html", null, true);
            echo ">
            <div class=\"sidebar\">
              ";
            // line 213
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])), "html", null, true);
            echo "
            </div>
          </div>
        ";
        }
        // line 217
        echo "        <!-- End: Right SideBar -->

      </div>

    </div>
  </div>
</div>

<!-- End: Main content -->


<!-- Start: Features -->

";
        // line 230
        if ((($this->getAttribute(($context["page"] ?? null), "features_first", []) || $this->getAttribute(($context["page"] ?? null), "features_second", [])) || $this->getAttribute(($context["page"] ?? null), "features_third", []))) {
            // line 231
            echo "
  <div class=\"features\">
    <div class=\"container\">
      <div class=\"row\">

        <!-- Start: Features First -->
        ";
            // line 237
            if ($this->getAttribute(($context["page"] ?? null), "features_first", [])) {
                // line 238
                echo "          <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["features_first_class"] ?? null)), "html", null, true);
                echo ">
            ";
                // line 239
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "features_first", [])), "html", null, true);
                echo "
          </div>
        ";
            }
            // line 242
            echo "        <!-- End: Features First -->

        <!-- Start :Features Second -->
        ";
            // line 245
            if ($this->getAttribute(($context["page"] ?? null), "features_second", [])) {
                // line 246
                echo "          <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["features_class"] ?? null)), "html", null, true);
                echo ">
            ";
                // line 247
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "features_second", [])), "html", null, true);
                echo "
          </div>
        ";
            }
            // line 250
            echo "        <!-- End: Features Second -->

        <!-- Start: Features third -->
        ";
            // line 253
            if ($this->getAttribute(($context["page"] ?? null), "features_third", [])) {
                // line 254
                echo "          <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["features_class"] ?? null)), "html", null, true);
                echo ">
            ";
                // line 255
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "features_third", [])), "html", null, true);
                echo "
          </div>
        ";
            }
            // line 258
            echo "        <!-- End: Features Third -->

      </div>
    </div>
  </div>

";
        }
        // line 265
        echo "
<!--End: Features -->


<!-- Start: Services -->

";
        // line 271
        if ($this->getAttribute(($context["page"] ?? null), "services", [])) {
            // line 272
            echo "
  <div class=\"services\" id=\"services\">
    <div class=\"container\">
      ";
            // line 275
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "services", [])), "html", null, true);
            echo "
    </div>
  </div>

";
        }
        // line 280
        echo "
<!--End: Services -->



<!-- Start: Services -->

";
        // line 287
        if ($this->getAttribute(($context["page"] ?? null), "products", [])) {
            // line 288
            echo "
  <!-- <div class=\"products\" id=\"products\">
    <div class=\"container\">
      ";
            // line 291
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "products", [])), "html", null, true);
            echo "
    </div>
  </div> -->

";
        }
        // line 296
        echo "
<!--End: Services -->


<!-- Start: Price table widgets -->

";
        // line 302
        if ((($this->getAttribute(($context["page"] ?? null), "pricetable_first", []) || $this->getAttribute(($context["page"] ?? null), "pricetable_second", [])) || $this->getAttribute(($context["page"] ?? null), "pricetable_third", []))) {
            // line 303
            echo "
  <div class=\"price-table\" id=\"price-table\">
    <div class=\"container\">
      <div class=\"row\">

        <!-- Start: Bottom First -->
        ";
            // line 309
            if ($this->getAttribute(($context["page"] ?? null), "pricetable_first", [])) {
                // line 310
                echo "          <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["pricetable_class"] ?? null)), "html", null, true);
                echo ">
            ";
                // line 311
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "pricetable_first", [])), "html", null, true);
                echo "
          </div>
        ";
            }
            // line 314
            echo "        <!-- End: Bottom First -->

        <!-- Start: Bottom Second -->
        ";
            // line 317
            if ($this->getAttribute(($context["page"] ?? null), "pricetable_second", [])) {
                // line 318
                echo "          <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["pricetable_class"] ?? null)), "html", null, true);
                echo ">
            ";
                // line 319
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "pricetable_second", [])), "html", null, true);
                echo "
          </div>
        ";
            }
            // line 322
            echo "        <!-- End: Bottom Second -->

        <!-- Start: Bottom third -->
        ";
            // line 325
            if ($this->getAttribute(($context["page"] ?? null), "pricetable_third", [])) {
                // line 326
                echo "          <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["pricetable_class"] ?? null)), "html", null, true);
                echo ">
            ";
                // line 327
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "pricetable_third", [])), "html", null, true);
                echo "
          </div>
        ";
            }
            // line 330
            echo "        <!-- End: Bottom Third -->

        <!-- Start: Bottom third -->
        ";
            // line 333
            if ($this->getAttribute(($context["page"] ?? null), "pricetable_forth", [])) {
                // line 334
                echo "          <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["pricetable_class"] ?? null)), "html", null, true);
                echo ">
            ";
                // line 335
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "pricetable_forth", [])), "html", null, true);
                echo "
          </div>
        ";
            }
            // line 338
            echo "        <!-- End: Bottom Third -->

      </div>
    </div>
  </div>

";
        }
        // line 345
        echo "
<!--End: Price table widgets -->


<!-- Start: Bottom widgets -->

";
        // line 351
        if (((($this->getAttribute(($context["page"] ?? null), "bottom_first", []) || $this->getAttribute(($context["page"] ?? null), "bottom_second", [])) || $this->getAttribute(($context["page"] ?? null), "bottom_third", [])) || $this->getAttribute(($context["page"] ?? null), "bottom_forth", []))) {
            // line 352
            echo "
  <div class=\"bottom-widget\" id=\"bottom-widget\">
    <div class=\"container\">
      <div class=\"row\">

        <!-- Start: Bottom First -->
        ";
            // line 358
            if ($this->getAttribute(($context["page"] ?? null), "bottom_first", [])) {
                // line 359
                echo "          <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bottom_class"] ?? null)), "html", null, true);
                echo ">
            ";
                // line 360
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "bottom_first", [])), "html", null, true);
                echo "
          </div>
        ";
            }
            // line 363
            echo "        <!-- End: Bottom First -->

        <!-- Start: Bottom Second -->
        ";
            // line 366
            if ($this->getAttribute(($context["page"] ?? null), "bottom_second", [])) {
                // line 367
                echo "          <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bottom_class"] ?? null)), "html", null, true);
                echo ">
            ";
                // line 368
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "bottom_second", [])), "html", null, true);
                echo "
          </div>
        ";
            }
            // line 371
            echo "        <!-- End: Bottom Second -->

        <!-- Start: Bottom third -->
        ";
            // line 374
            if ($this->getAttribute(($context["page"] ?? null), "bottom_third", [])) {
                // line 375
                echo "          <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bottom_class"] ?? null)), "html", null, true);
                echo ">
            ";
                // line 376
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "bottom_third", [])), "html", null, true);
                echo "
          </div>
        ";
            }
            // line 379
            echo "        <!-- End: Bottom Third -->

        <!-- Start: Bottom Forth -->
        ";
            // line 382
            if ($this->getAttribute(($context["page"] ?? null), "bottom_forth", [])) {
                // line 383
                echo "          <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bottom_class"] ?? null)), "html", null, true);
                echo ">
            ";
                // line 384
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "bottom_forth", [])), "html", null, true);
                echo "
          </div>
        ";
            }
            // line 387
            echo "        <!-- End: Bottom Forth -->

      </div>
    </div>
  </div>

";
        }
        // line 394
        echo "
<!--End: Bottom widgets -->


<!-- Start: Team widgets -->

";
        // line 400
        if (((($this->getAttribute(($context["page"] ?? null), "team_first", []) || $this->getAttribute(($context["page"] ?? null), "team_second", [])) || $this->getAttribute(($context["page"] ?? null), "team_third", [])) || $this->getAttribute(($context["page"] ?? null), "team_forth", []))) {
            // line 401
            echo "
  <div class=\"team\" id=\"team\">
    <div class=\"container\">
      <div class=\"row\">

        <!-- Start: Bottom First -->
        ";
            // line 407
            if ($this->getAttribute(($context["page"] ?? null), "team_first", [])) {
                // line 408
                echo "          <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["team_class"] ?? null)), "html", null, true);
                echo ">
            ";
                // line 409
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "team_first", [])), "html", null, true);
                echo "
          </div>
        ";
            }
            // line 412
            echo "        <!-- End: Bottom First -->

        <!-- Start: Bottom Second -->
        ";
            // line 415
            if ($this->getAttribute(($context["page"] ?? null), "team_second", [])) {
                // line 416
                echo "          <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["team_class"] ?? null)), "html", null, true);
                echo ">
            ";
                // line 417
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "team_second", [])), "html", null, true);
                echo "
          </div>
        ";
            }
            // line 420
            echo "        <!-- End: Bottom Second -->

        <!-- Start: Bottom third -->
        ";
            // line 423
            if ($this->getAttribute(($context["page"] ?? null), "team_third", [])) {
                // line 424
                echo "          <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["team_class"] ?? null)), "html", null, true);
                echo ">
            ";
                // line 425
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "team_third", [])), "html", null, true);
                echo "
          </div>
        ";
            }
            // line 428
            echo "        <!-- End: Bottom Third -->

        <!-- Start: Bottom Forth -->
        ";
            // line 431
            if ($this->getAttribute(($context["page"] ?? null), "team_forth", [])) {
                // line 432
                echo "          <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["team_class"] ?? null)), "html", null, true);
                echo ">
            ";
                // line 433
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "team_forth", [])), "html", null, true);
                echo "
          </div>
        ";
            }
            // line 436
            echo "        <!-- End: Bottom Forth -->

      </div>
    </div>
  </div>

";
        }
        // line 443
        echo "
<!--End: Team widgets -->


<!-- Start: Footer widgets -->

";
        // line 449
        if ((($this->getAttribute(($context["page"] ?? null), "footer_first", []) || $this->getAttribute(($context["page"] ?? null), "footer_second", [])) || $this->getAttribute(($context["page"] ?? null), "footer_third", []))) {
            // line 450
            echo "
  <div class=\"footer\" id=\"footer\">
    <div class=\"container\">
      <div class=\"parallax-region wow- bounceInUp\">
        <div class=\"row\">

          <!-- Start: Footer First -->
          ";
            // line 457
            if ($this->getAttribute(($context["page"] ?? null), "footer_first", [])) {
                // line 458
                echo "            <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["footer_class"] ?? null)), "html", null, true);
                echo ">
              ";
                // line 459
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_first", [])), "html", null, true);
                echo "
            </div>
          ";
            }
            // line 462
            echo "          <!-- End: Footer First -->

          <!-- Start :Footer Second -->
          ";
            // line 465
            if ($this->getAttribute(($context["page"] ?? null), "footer_second", [])) {
                // line 466
                echo "            <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["footer_class"] ?? null)), "html", null, true);
                echo ">
              ";
                // line 467
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_second", [])), "html", null, true);
                echo "
            </div>
          ";
            }
            // line 470
            echo "          <!-- End: Footer Second -->

          <!-- Start: Footer third -->
          ";
            // line 473
            if ($this->getAttribute(($context["page"] ?? null), "footer_third", [])) {
                // line 474
                echo "            <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["footer_class"] ?? null)), "html", null, true);
                echo ">
              ";
                // line 475
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_third", [])), "html", null, true);
                echo "
            </div>
          ";
            }
            // line 478
            echo "          <!-- End: Footer Third -->

        </div>
      </div>
    </div>
  </div>

";
        }
        // line 486
        echo "
<!--End: Footer widgets -->

<!-- Start: Copyright -->

<div class=\"copyright\">

    <div class=\"container\">

      ";
        // line 495
        if (($context["show_social_icon"] ?? null)) {
            // line 496
            echo "        <p class=\"social-media\">
          ";
            // line 497
            if (($context["facebook_url"] ?? null)) {
                // line 498
                echo "            <a href=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["facebook_url"] ?? null)), "html", null, true);
                echo "\"  class=\"facebook\" target=\"_blank\" ><i class=\"fab fa-facebook-f\"></i></a>
          ";
            }
            // line 500
            echo "          ";
            if (($context["google_plus_url"] ?? null)) {
                // line 501
                echo "            <a href=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["google_plus_url"] ?? null)), "html", null, true);
                echo "\"  class=\"google-plus\" target=\"_blank\" ><i class=\"fab fa-google-plus-g\"></i></a>
          ";
            }
            // line 503
            echo "          ";
            if (($context["twitter_url"] ?? null)) {
                // line 504
                echo "            <a href=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["twitter_url"] ?? null)), "html", null, true);
                echo "\" class=\"twitter\" target=\"_blank\" ><i class=\"fab fa-twitter\"></i></a>
          ";
            }
            // line 506
            echo "          ";
            if (($context["linkedin_url"] ?? null)) {
                // line 507
                echo "            <a href=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["linkedin_url"] ?? null)), "html", null, true);
                echo "\" class=\"linkedin\" target=\"_blank\"><i class=\"fab fa-linkedin-in\"></i></a>
          ";
            }
            // line 509
            echo "          ";
            if (($context["pinterest_url"] ?? null)) {
                // line 510
                echo "            <a href=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["pinterest_url"] ?? null)), "html", null, true);
                echo "\" class=\"pinterest\" target=\"_blank\" ><i class=\"fab fa-pinterest-p\"></i></a>
          ";
            }
            // line 512
            echo "          ";
            if (($context["rss_url"] ?? null)) {
                // line 513
                echo "            <a href=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["rss_url"] ?? null)), "html", null, true);
                echo "\" class=\"rss\" target=\"_blank\" ><i class=\"fa fa-rss\"></i></a>
          ";
            }
            // line 515
            echo "        </p>
      ";
        }
        // line 517
        echo "
      <p>Copyright © ";
        // line 518
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
        echo ". All rights reserved.</p>

      ";
        // line 520
        if (($context["show_credit_link"] ?? null)) {
            // line 521
            echo "        <p class=\"credit-link\">Designed By <a href=\"http://www.zymphonies.com\" target=\"_blank\">Zymphonies</a></p>
      ";
        }
        // line 523
        echo "
  </div>

</div>

<!-- End: Copyright -->





";
    }

    public function getTemplateName()
    {
        return "themes/startup_zymphonies_theme/templates/layout/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  923 => 523,  919 => 521,  917 => 520,  912 => 518,  909 => 517,  905 => 515,  899 => 513,  896 => 512,  890 => 510,  887 => 509,  881 => 507,  878 => 506,  872 => 504,  869 => 503,  863 => 501,  860 => 500,  854 => 498,  852 => 497,  849 => 496,  847 => 495,  836 => 486,  826 => 478,  820 => 475,  815 => 474,  813 => 473,  808 => 470,  802 => 467,  797 => 466,  795 => 465,  790 => 462,  784 => 459,  779 => 458,  777 => 457,  768 => 450,  766 => 449,  758 => 443,  749 => 436,  743 => 433,  738 => 432,  736 => 431,  731 => 428,  725 => 425,  720 => 424,  718 => 423,  713 => 420,  707 => 417,  702 => 416,  700 => 415,  695 => 412,  689 => 409,  684 => 408,  682 => 407,  674 => 401,  672 => 400,  664 => 394,  655 => 387,  649 => 384,  644 => 383,  642 => 382,  637 => 379,  631 => 376,  626 => 375,  624 => 374,  619 => 371,  613 => 368,  608 => 367,  606 => 366,  601 => 363,  595 => 360,  590 => 359,  588 => 358,  580 => 352,  578 => 351,  570 => 345,  561 => 338,  555 => 335,  550 => 334,  548 => 333,  543 => 330,  537 => 327,  532 => 326,  530 => 325,  525 => 322,  519 => 319,  514 => 318,  512 => 317,  507 => 314,  501 => 311,  496 => 310,  494 => 309,  486 => 303,  484 => 302,  476 => 296,  468 => 291,  463 => 288,  461 => 287,  452 => 280,  444 => 275,  439 => 272,  437 => 271,  429 => 265,  420 => 258,  414 => 255,  409 => 254,  407 => 253,  402 => 250,  396 => 247,  391 => 246,  389 => 245,  384 => 242,  378 => 239,  373 => 238,  371 => 237,  363 => 231,  361 => 230,  346 => 217,  339 => 213,  333 => 211,  331 => 210,  326 => 207,  319 => 203,  313 => 201,  311 => 200,  306 => 197,  299 => 193,  293 => 191,  291 => 190,  283 => 184,  277 => 181,  274 => 180,  272 => 179,  261 => 170,  253 => 165,  248 => 162,  246 => 161,  239 => 156,  232 => 152,  228 => 150,  226 => 149,  218 => 143,  210 => 137,  202 => 135,  200 => 134,  195 => 131,  187 => 129,  185 => 128,  180 => 125,  172 => 123,  170 => 122,  165 => 119,  157 => 117,  155 => 116,  148 => 111,  146 => 110,  140 => 106,  135 => 103,  126 => 101,  122 => 100,  118 => 98,  116 => 97,  106 => 89,  99 => 85,  96 => 84,  90 => 82,  88 => 81,  84 => 79,  82 => 78,  76 => 74,  70 => 72,  68 => 71,  55 => 60,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/startup_zymphonies_theme/templates/layout/page.html.twig", "D:\\Web-2023\\huyanh-safelife\\themes\\startup_zymphonies_theme\\templates\\layout\\page.html.twig");
    }
}
